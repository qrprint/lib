# Bibliothèque pour générer un fichier PDF avec une grille de QRCodes

**QRGridPDFlib** est une bibliothèque JavaScript qui vous permet de générer un fichier PDF avec une grille de QRCodes. 


## Utilisation

Vous pouvez intégrer **QRGridPDFlib** dans votre projet en important QRGridPDFlib.js.
Pour faire fonctionner cette bibliothèque, il faut aussi importer [QRCodeStyling](https://github.com/kozakdenys/qrcode-styling), [jsPDF](https://github.com/parallax/jsPDF) et [html2canvas](https://html2canvas.hertzen.com/).


### Création de la liste de configuration

Tout d'abord, préparez les listes de configurations de QR codes. Ces listes sont au format JSON.
Chaque configuration peut inclure des données, des options de style, des images, des titres et des données supplémentaires.
Deux listes sont nécessaire :
- la première, **qrCodesList**, permet de récupérer les données et les configurations d'un ou de plusieurs QRCode
- la deuxième, **gridOptions**, permet de configurer la sortie PDF (le nombre de lignes et colonnes de la grille, les marges de la page, le nombre de QR Code à représenter et leur disposition, ...)

Voici des exemples de ces deux listes :

```javascript
const qrCodesList = [
{
"data": "https://qrprint.forge.apps.education.fr/app/",     // URL ou données du QR Code
"includeDataBelow": false,                                  // Afficher ou non les données sous le QR (true/false)
"title": "Accès QRPrint",                                   // Texte du itre
"includeTitleFrame": true,                                  // Afficher le titre avec le cadre (true/false)
"frameColor": "#3498db",                                    // Couleur du cadre (code couleur hexadécimal)
"titleColor": "#ffffff",                                    // Couleur du titre (code couleur hexadécimal)
"backgroundColor": "#ffffff",                               // Couleur de fond du QR Code (code couleur hexadécimal)
"transparentBackground": false,                             // Fond transparent (true/false)
"dotsType": "dots",                                         // Type des points (square, dots, rounded, extra-rounded, classy, classy-rounded)
"dotsColor": "#ff0000",                                     // Couleur des points (code couleur hexadécimal)
"cornersSquareType": "square",                              // Type de l'intérieur des coins (square, dot, extra-rounded)
"cornersSquareColor": "##235475",                           // Couleur de l'intérieur des coins (code couleur hexadécimal)
"cornersDotType": "dot",                                    // Type du cadre des coins (square, dot)
"cornersDotColor": "##235475",                              // Couleur du cadre des coins(code couleur hexadécimal)
"errorCorrectionLevel": "Q",                                // Niveau de correction d'erreurs (L, M, Q, H)
"image": "",                                                // Lien vers une image
"imageOptions": {                                           // Options pour l'image au centre du QR Code
    "hideBackgroundDots": true,                             // Cacher les points de fond derrière l'image (true/false)
    "imageSize": 0.4,                                       // Taille de l'image par rapport au QR Code (0 à 1)
    "margin": 4                                             // Marge autour de l'image (en pixels)
},
"margin": 15,                                               // Marge autour du QR Code (en pixels)
"printNumber": 13                                           // Nombre d'impressions pour ce QR Code
    },
    {
"data": "https://forge.apps.education.fr",                  // Deuxième QR Code avec des options différentes
"includeDataBelow": true,
"title": "Visitez La Forge Edu",
"includeTitleFrame": true,
"frameColor": "#f17361",
"titleColor": "#000000",
"backgroundColor": "#ffffff",
"transparentBackground": false,
"dotsType": "square",
"dotsColor": "#000000",
"cornersSquareType": "square",
"cornersSquareColor": "#000000",
"cornersDotType": "dot",
"cornersDotColor": "#000000",
"errorCorrectionLevel": "Q",
"imageOptions": {
    "hideBackgroundDots": true,
    "imageSize": 0.4,
    "margin": 4
},
"margin": 15,
"printNumber": 7
    }
];
```


```javascript
const gridOptions = {
"rows": 6,                      // Nombre de lignes dans la grille
"columns": 4,                   // Nombre de colonnes dans la grille
 "dottedLines": true,           // Lignes pointillées affichées (true/false)
 "multiple": true,              // Plusieurs QR Codes différents (true/false)
 "globalPrintNumber": false,    // Utiliser un nombre global d'impressions identique pour chaque QR (true/false)
 "globalPrintNumberValue": 1,   // Nombre d'impressions global si globalPrintNumber est true
 "placementType": "grouped",    // Type de placement des QR Codes (alternated, grouped) si globalPrintNumber est true
 "lineBreak": true              // Sauter des lignes dans la grille pour les QR multiples à chaque changement (true/false)
};

```

### Génération du PDF

Utilisez la fonction `generateQRCodeGridPDF` pour créer un PDF à partir des QR codes.

```javascript
<iframe id="pdfViewer" class="pdf-viewer"></iframe>
<script>
generateQRCodeGridPDF(qrCodesList, gridOptions)
  .then((blob) => {
    const url = URL.createObjectURL(blob);
    const pdfViewer = document.getElementById('pdfViewer');
    pdfViewer.src = url;
  })
  .catch((error) => {
    console.error('Erreur lors de la génération du PDF:', error);
  });
</script>
```

## Options

### Options du QR Code

Chaque configuration de QR code peut inclure les options suivantes :

| Option                  | Type      | Description                                                                                           | Valeur par Défaut |
|-------------------------|-----------|-------------------------------------------------------------------------------------------------------|-------------------|
| `data`                  | `string`  | Les données à encoder dans le QR code.                                                                | `'QRPrint'`       |
| `includeDataBelow`      | `boolean` | Indique si des données doivent être incluses dans un cadre sous le QR code (true/false).              | `false`           |
| `title`                 | `string`  | Le texte du titre à afficher au-dessous du QR code.                                                   | `''`              |
| `includeTitleFrame`     | `boolean` | Indique si un cadre de titre doit être inclus autour du QR code (true/false).                         | `false`           |
| `frameColor`            | `string`  | La couleur du cadre entourant le QR code (code couleur hexadécimal).                                  | `'#000000'`       |
| `titleColor`            | `string`  | La couleur du texte du titre (code couleur hexadécimal).                                              | `'#ffffff'`       |
| `backgroundColor`       | `string`  | La couleur de fond du QR Code (code couleur hexadécimal).                                             | `'#ffffff'`       |
| `transparentBackground` | `boolean` | Indique si le fond du QR code est transparent (true/false).                                           | `false`           |
| `dotsType`              | `string`  | Le type de points(`'square'`, `'rounded'`, `'dots'`, `'extra-rounded'`,`'classy'`,`'classy-rounded'`).| `'square'`        |
| `dotsColor`             | `string`  | La couleur des points du QR code (code couleur hexadécimal).                                          | `'#000000'`       |
| `cornersSquareType`     | `string`  | Le type de l'intérieur des coins (`'square'`, `'dot'`, `'extra-rounded'`).                            | `'square'`        |
| `cornersSquareColor`    | `string`  | La couleur de l'intérieur des coins (code couleur hexadécimal).                                       | `'#000000'`       |
| `cornersDotType`        | `string`  | Le type du cadre des coins (`'dot'`, `'square'`).                                                     | `'square'`        |
| `cornersDotColor`       | `string`  | La couleur du cadre des coins (code couleur hexadécimal).                                             | `'#000000'`       |
| `errorCorrectionLevel`  | `string`  | Le niveau de correction d'erreurs (`'L'`, `'M'`, `'Q'`, `'H'`).                                       | `'Q'`             |
| `margin`                | `integer` | La marge en pixel du QR Code à l'intérieur du cadre.                                                  | `'0'`             |
| `printNumber`           | `integer` | Le nombre de QR Codes à représenter s'il y en a plusieurs et que l'option globalPrintNumber est false.| `'1'`             |
| `image`                 | `string`  | L'URL de l'image à intégrer dans le QR code.                                                          | `undefined`       |
| `imageOptions`          | `object`  | Options pour l'image intégrée (liste).                                                                | `{}`              |
|    `hideBackgroundDots` | `boolean` | Indique si les points derrière l'image sont masqués (true/false).                                     | `true`            |
|    `imageSize`          | `float`   | Définit le coefficient de taille de l'image (nombre entre 0 et 1).                                    | `0.4`             |
|    `margin`             | `integer` | Définit la marge autour de l'image (la taille globale ne change pas).                                 | `4`               |


#### Options du PDF Généré

| Option                  | Type      | Description                                                                           | Valeur par Défaut |
|-------------------------|-----------|---------------------------------------------------------------------------------------|-------------------|
| `rows`                  | `integer` | Nombre de lignes dans la grille de QR codes.                                          | `6`               |
| `columns`               | `integer` | Nombre de colonnes dans la grille de QR codes.                                        | `5`               |
| `dottedLines`           | `boolean` | Indique si des lignes pointillées doivent être dessinées entre les QR codes.          | `true`            |
| `multiple`              | `boolean` | Indique si plusieurs QR Codes doivent être représentés.                               | `false`           |
| `globalPrintNumber`     | `boolean` | Indique si un nombre unique est utilisés pour représenter tous les QR codes.          | `false`           |
| `globalPrintNumberValue`| `integer` | Nombre de QR Codes à représenter quand globalPrintNumber est à true.                  | `1`               |
| `placementType`         | `string`  | Type de placement quand globalPrintNumber est à true (`'alternated'`, `'sequential'`).| `alternated`      |
| `lineBreak`             | `boolean` | Indique s'il y a un retour à la ligne après chaque QR Code différent.                 | `false`           |
| `marginPage`            | `integer` | Définit la marge de la page.                                                          | `0`               |

## Dépendances

* [QRCodeStyling](https://github.com/kozakdenys/qrcode-styling)
* [jsPDF](https://github.com/parallax/jsPDF)
* [html2canvas](https://html2canvas.hertzen.com/)

## Licence
Ce projet est sous licence MIT.