function getTextWidth(text, font) {
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
}
function adjustFontSize(textElement) {
    let fontSize = 100;
    textElement.style.fontSize = fontSize + '%';
    let taille = getTextWidth(textElement.innerText, fontSize * 2 / 100 + 'rem Arial');
    while (taille > 150 && fontSize > 50) {
        fontSize -= 1;
        textElement.style.fontSize = fontSize + '%';
        taille = getTextWidth(textElement.innerText, fontSize * 2 / 100 + 'rem Arial');
    }
}

async function fetchImageAsBlob(url) {
    const response = await fetch(url, { mode: 'cors' });
    if (!response.ok) {
        throw new Error(`Erreur lors du téléchargement de l'image: ${response.statusText}`);
    }
    const blob = await response.blob();
    return blob;
}

function blobToDataURL(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = () => resolve(reader.result);
        reader.onerror = () => reject(new Error('Erreur lors de la lecture du Blob.'));
        reader.readAsDataURL(blob);
    });
}


async function generateQRCode(qrCodeList) {
    const styleSheet = document.createElement('style');
    styleSheet.type = 'text/css';
    styleSheet.innerHTML = `
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    `;
    document.head.appendChild(styleSheet);
    const loadingOverlay = document.createElement('div');
    Object.assign(loadingOverlay.style, {
        position: 'fixed',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: '1000',
    });
    document.body.appendChild(loadingOverlay);
    const loadingSpinner = document.createElement('div');
    Object.assign(loadingSpinner.style, {
        border: '16px solid #3498db',
        borderRadius: '50%',
        borderTop: '16px solid #235475',
        width: '120px',
        height: '120px',
        animation: 'spin 2s linear infinite',
    });
    loadingOverlay.appendChild(loadingSpinner);
    const imagesArray = [];
    const dimensionsArray = [];
    const container = document.createElement('div');
    container.id = 'qrCodeContainer';
    container.style.position = 'absolute';
    container.style.top = '9999px';
    document.body.appendChild(container);
    Object.assign(container.style, {
        width: '350px',
    });
    for (let index = 0; index < qrCodeList.length; index++) {
        const qrCodeItem = qrCodeList[index];
        container.innerHTML = '';
        const qrContainer = document.createElement('div');
        Object.assign(qrContainer.style, {
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            width: '350px',
            marginLeft: 'auto',
            marginRight: 'auto',
            fontFamily: "'Arial', sans-serif",
        });
        const qrPreview = document.createElement('div');
        Object.assign(qrPreview.style, {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            border: `12px solid ${qrCodeItem.frameColor || '#000'}`,
            borderTopLeftRadius: '1em',
            borderTopRightRadius: '1em',
            zIndex: '1',
            overflow: 'hidden',
            width: '300px',
        });
        qrPreview.style.setProperty('border-bottom', 'none', 'important');
        const qrCode = new QRCodeStyling({
            width: 300,
            height: 300,
            data: qrCodeItem.data || 'QRPrint',
            margin: qrCodeItem.margin || 0,
            dotsOptions: {
                color: qrCodeItem.dotsColor || '#000000',
                type: qrCodeItem.dotsType || 'square'
            },
            cornersSquareOptions: {
                color: qrCodeItem.cornersSquareColor || '#000000',
                type: qrCodeItem.cornersSquareType || 'square'
            },
            cornersDotOptions: {
                color: qrCodeItem.cornersDotColor || '#000000',
                type: qrCodeItem.cornersDotType || 'dot'
            },
            backgroundOptions: {
                color: qrCodeItem.transparentBackground ? 'rgba(255,255,255,0)' : (qrCodeItem.backgroundColor || '#ffffff'),
                transparent: qrCodeItem.transparentBackground || false
            },
            qrOptions: {
                errorCorrectionLevel: qrCodeItem.errorCorrectionLevel || 'Q'
            },
            imageOptions: {
                hideBackgroundDots: qrCodeItem.imageOptions?.hideBackgroundDots || true,
                imageSize: qrCodeItem.imageOptions?.imageSize || 0.4,
                margin: qrCodeItem.imageOptions?.margin || 4
            },
            image: qrCodeItem.image || '',
        });

        if (qrCodeItem.image) {
            try {
                const blob = await fetchImageAsBlob(qrCodeItem.image);
                
                const dataURL = await blobToDataURL(blob);
                
                await qrCode.update({
                    image: dataURL
                });

                qrCode.append(qrPreview);

            } catch (error) {
                console.error(`Erreur lors du traitement de l'image pour le QR Code ${index + 1}:`, error);
            }
        }
        else{
         qrCode.append(qrPreview);
        }
        

        qrContainer.appendChild(qrPreview);
        if (qrCodeItem.includeTitleFrame) {
            const titleFrame = document.createElement('div');
            Object.assign(titleFrame.style, {
                padding: '10px 15px',
                textAlign: 'center',
                fontWeight: 'bold',
                position: 'relative',
                backgroundColor: qrCodeItem.frameColor || '#000000',
                color: qrCodeItem.titleColor || '#ffffff',
                border: `5px solid ${qrCodeItem.frameColor || '#000000'}`,
                borderBottomLeftRadius: '1em',
                borderBottomRightRadius: '1em',
                fontSize: '2rem',
                width: '324px',
                overflow: 'hidden',
                boxSizing: 'border-box',
                height: '60px',
            });
            const titleText = document.createElement('div');
            titleText.id = `qrTitleText`;
            titleTextCut = (qrCodeItem.title).substring(0, 25);
            titleText.textContent = titleTextCut || '';
            Object.assign(titleText.style, {
                whiteSpace: 'nowrap',
            });
            titleFrame.appendChild(titleText);
            qrContainer.appendChild(titleFrame);
        } else {
            Object.assign(qrPreview.style, {
                border: 'none',
            });
        }
        if (qrCodeItem.includeDataBelow) {
            const dataFrame = document.createElement('div');
            Object.assign(dataFrame.style, {
                padding: '15px 5px',
                textAlign: 'center',
                fontWeight: 'bold',
                position: 'relative',
                marginTop: '5px',
                border: '2px solid #aaaaaa',
                backgroundColor: '#ffffff',
                color: '#000000',
                width: '350px',
                overflow: 'hidden',
                boxSizing: 'border-box',
            });
            const dataText = document.createElement('div');
            dataText.id = `qrDataText`;
            dataText.textContent = qrCodeItem.data;
            Object.assign(dataText.style, {
                display: 'inline-block !important',
            });
            dataText.style.setProperty('white-space', 'pre-wrap', 'important');
            dataText.style.setProperty('word-break', 'break-all', 'important');
            dataText.style.setProperty('font-size', '20px', 'important');
            const formattedDataText = dataText.textContent.replace(/(.{32})/g, "$1\n");
            dataText.textContent = formattedDataText;
            dataFrame.appendChild(dataText);
            qrContainer.appendChild(dataFrame);
        }
        container.appendChild(qrContainer);
        if (qrCodeItem.includeTitleFrame) {
            const titleTextElement = document.getElementById(`qrTitleText`);
            adjustFontSize(titleTextElement);
        }

        await qrCode.getRawData('png');
        await new Promise(resolve => setTimeout(resolve, 100));
        const canvas = await html2canvas(qrContainer, {
            backgroundColor: null
        });
        const imgData = canvas.toDataURL('image/png');
        imagesArray.push(imgData);
        const imgDimensions = {
            width: canvas.width,
            height: canvas.height
        };
        dimensionsArray.push(imgDimensions);
    }
    document.body.removeChild(container);
    document.body.removeChild(loadingOverlay);
    return {
        imagesArray,
        dimensionsArray
    };
}

function drawFullWidthLine(pdf, y) {
    const startX = 0;
    const endX = pdf.internal.pageSize.width;
    drawDottedLine(pdf, startX, y, endX, y);
}

function drawFullHeightLine(pdf, x) {
    const startY = 0;
    const endY = pdf.internal.pageSize.height;
    drawDottedLine(pdf, x, startY, x, endY);
}

function drawDottedLine(pdf, x1, y1, x2, y2) {
    const dashLength = 2;
    const gapLength = 2;
    const totalLength = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    const dashes = Math.floor(totalLength / (dashLength + gapLength));
    for (let i = 0; i < dashes; i++) {
        const x = x1 + (i * (dashLength + gapLength) * (x2 - x1) / totalLength);
        const y = y1 + (i * (dashLength + gapLength) * (y2 - y1) / totalLength);
        pdf.line(x, y, x + dashLength * (x2 - x1) / totalLength, y + dashLength * (y2 - y1) / totalLength);
    }
}

async function generateQRCodeGridPDF(qrCodesList, gridOptions) {
    const pdf = new jsPDF({
        orientation: 'portrait',
        unit: 'mm',
        format: 'a4',
        putOnlyUsedFonts: true,
        floatPrecision: 16
    });
    const rows = gridOptions.rows || 6;
    const columns = gridOptions.columns || 5;
    const dottedlinesValue = gridOptions.dottedLines || false;
    const multiple = gridOptions.multiple || false;
    const globalPrint = gridOptions.globalPrintNumber || false;
    const globalNumber = gridOptions.globalPrintNumberValue || 1;
    const placementType = gridOptions.placementType || 'alternated';
    const linebreak = gridOptions.lineBreak || false;
    const margin = gridOptions.marginPage || 0;
    const pageWidth = pdf.internal.pageSize.width;
    const pageHeight = pdf.internal.pageSize.height;
    let cellWidth = (pageWidth - (margin * 2)) / columns;
    let cellHeight = (pageHeight - (margin * 2)) / rows;
    const widthArray = [];
    const heightArray = [];
    const imgDataArray = [];
    const printNumbers = [];
    const {
        imagesArray,
        dimensionsArray
    } = await generateQRCode(qrCodesList);
    for (let i = 0; i < imagesArray.length; i++) {
        imgDataArray.push(imagesArray[i]);
        widthArray.push(dimensionsArray[i].width);
        heightArray.push(dimensionsArray[i].height);
        if (globalPrint) {
            printNumbers.push(globalNumber);
        } else {
            printNumbers.push(qrCodesList[i].printNumber || 1);
        }
    }
    const total = imgDataArray.length;
    if (multiple) {
        let number = 0;
        let temp_number = 1;
        let total_print = 0;
        printNumbers.forEach(nombre => {
            total_print += nombre;
        });
        const nb_page = Math.floor((total_print - 1) / (rows * columns)) + 1;
        outerLoop: for (let n = 0; n < nb_page; n++) {
            for (let i = 0; i < rows; i++) {
                let y = i * cellHeight + margin;
                if (dottedlinesValue) drawFullWidthLine(pdf, y);
                innerLoop: for (let j = 0; j < columns; j++) {
                    const x = j * cellWidth + margin;
                    const imgIndex = number % total;
                    const imgWidth = widthArray[imgIndex];
                    const imgHeight = heightArray[imgIndex];
                    const imgData = imgDataArray[imgIndex];
                    let aspectRatio = imgWidth / imgHeight;
                    let drawWidth = cellWidth - 5;
                    let drawHeight = drawWidth / aspectRatio;
                    if (drawHeight > cellHeight - 5) {
                        drawHeight = cellHeight - 5;
                        drawWidth = drawHeight * aspectRatio;
                    }
                    const xOffset = x + (cellWidth - drawWidth) / 2;
                    const yOffset = y + (cellHeight - drawHeight) / 2;
                    pdf.addImage(imgData, 'PNG', xOffset, yOffset, drawWidth, drawHeight);
                    if (dottedlinesValue) drawFullHeightLine(pdf, x);
                    temp_number++;
                    if (temp_number > printNumbers[imgIndex]) {
                        number++;
                        temp_number = 1;
                        if (linebreak) break innerLoop;
                    }
                    if (number >= total) break outerLoop;
                }
                if (number >= total) break outerLoop;
            }
            if (dottedlinesValue) {
            drawFullWidthLine(pdf, rows * cellHeight + margin);
            drawFullHeightLine(pdf, columns * cellWidth + margin);
        }
            if (n < nb_page - 1) pdf.addPage();
            
        }
        
    } else {
        const imgData = imgDataArray[0];
        const imgWidth = widthArray[0];
        const imgHeight = heightArray[0];
        for (let i = 0; i < rows; i++) {
            let y = i * cellHeight + margin;
            if (dottedlinesValue) drawFullWidthLine(pdf, y);
            for (let j = 0; j < columns; j++) {
                const x = j * cellWidth + margin;
                let aspectRatio = imgWidth / imgHeight;
                let drawWidth = cellWidth - 5;
                let drawHeight = drawWidth / aspectRatio;
                if (drawHeight > cellHeight - 5) {
                    drawHeight = cellHeight - 5;
                    drawWidth = drawHeight * aspectRatio;
                }
                const xOffset = x + (cellWidth - drawWidth) / 2;
                const yOffset = y + (cellHeight - drawHeight) / 2;
                pdf.addImage(imgData, 'PNG', xOffset, yOffset, drawWidth, drawHeight);
                if (dottedlinesValue) drawFullHeightLine(pdf, x);
            }
            if (dottedlinesValue) {
            drawFullWidthLine(pdf, rows * cellHeight + margin);
            drawFullHeightLine(pdf, columns * cellWidth + margin);
        }
        }
        
    }

    const blob = pdf.output('blob');
    return blob;
}